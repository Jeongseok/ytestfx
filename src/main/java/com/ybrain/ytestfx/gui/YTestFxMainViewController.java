package com.ybrain.ytestfx.gui;

import com.ybrain.ytestfx.FxControl;
import com.ybrain.ytestfx.FxControlType;
import com.ybrain.ytestfx.FxTestCase;
import com.ybrain.ytestfx.FxTestSuite;
import com.ybrain.ytestfx.parser.FxmlParser;
import com.ybrain.ytestfx.testcase.FxTestCaseCombinator;
import com.ybrain.ytestfx.testcode.TestFxCodeGenerator;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;

/**
 * Created by jeongseok on 2016-05-25.
 */
public class YTestFxMainViewController extends Pane implements Initializable, Cloneable {

    @FXML
    private TableView fxTvControl;
    @FXML
    private Label fxLabelControlInfo;
    @FXML
    private TableView fxTvTestCase;

    @FXML
    private Label fxTestCodePath;

    private TableColumn mTcCheck;
    private TableColumn mTcControlType;
    private TableColumn mTcControlId;
    private TableColumn mTcGenerateBtn;

    private TableColumn mTcCaseCheck;
    private TableColumn mTcSequence;
    private TableColumn mTcResultDemo;

    private ObservableList<ControlListModel> mControlList;
    private ObservableList<TestCaseListModel> mTestCaseList;
    private File mFxmlFile;

    public YTestFxMainViewController() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("YTestFxAppView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initVariable();
    }

    public void addControlList(List<FxControl> fxControlList) {
        mControlList.clear();
        for (int i = 0; i < fxControlList.size(); i++) {
            mControlList.add(new ControlListModel(fxControlList.get(i)));
        }
    }

    private void initVariable() {
        mControlList = FXCollections.observableArrayList();
        mTestCaseList = FXCollections.observableArrayList();

        mTcCheck = new TableColumn("Check");
        mTcControlType = new TableColumn("Control Type");
        mTcControlId = new TableColumn("fx:id");
        mTcGenerateBtn = new TableColumn("Test Data");

        mTcCheck.setCellFactory(p -> new CheckBoxCell());
        mTcControlType.setCellValueFactory(new PropertyValueFactory<ControlListModel, String>("controlType"));
        mTcControlId.setCellValueFactory(new PropertyValueFactory<ControlListModel, String>("controlId"));
        mTcGenerateBtn.setCellFactory(p -> new BtnTestDataGenCell());

        fxTvControl.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        fxTvControl.getColumns().clear();
        fxTvControl.getColumns().addAll(mTcCheck, mTcControlId, mTcControlType, mTcGenerateBtn);
        fxTvControl.setItems(mControlList);

        mTcCaseCheck = new TableColumn("Check");
        mTcSequence = new TableColumn("Sequence");
        mTcResultDemo = new TableColumn("Result Demo");

        mTcCaseCheck.setCellFactory(p -> new CaseCheckBoxCell());
        mTcSequence.setCellValueFactory(new PropertyValueFactory<TestCaseListModel, String>("testCaseSequence"));
        mTcResultDemo.setCellValueFactory(new PropertyValueFactory<TestCaseListModel, String>("testCaseResultDemo"));
        mTcResultDemo.setCellFactory(TextFieldTableCell.forTableColumn());
        mTcResultDemo.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<TestCaseListModel, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<TestCaseListModel, String> t) {
                t.getTableView().getItems().get(
                        t.getTablePosition().getRow()).setTestCaseResultDemo(t.getNewValue());
            }
        });

        fxTvTestCase.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        fxTvTestCase.getColumns().clear();
        fxTvTestCase.getColumns().addAll(mTcCaseCheck, mTcSequence, mTcResultDemo);
        fxTvTestCase.setItems(mTestCaseList);
    }

    @FXML
    private void handleTableViewControlClicked() {
        fxLabelControlInfo.setText("");
        int selectedIndex = fxTvControl.getSelectionModel().getSelectedIndex();
        if (selectedIndex < 0)
            return;

        ControlListModel cm = (ControlListModel) fxTvControl.getSelectionModel().getSelectedItem();
        List<String> testDataList = cm.getTestDataList();
        if (testDataList != null) {
            fxLabelControlInfo.setText(testDataList.toString());
        }
    }

    Map<String, FxControl> mFxControlUnderTestMap = null;

    @FXML
    private void onGenTestCaseMouseClicked() {

        if (mControlList == null || mControlList.size() == 0)
            return;

        // Get selected controls
        List<ControlListModel> selectedControls = new ArrayList<>();
        for (ControlListModel control : mControlList) {
            if (control.isChecked())
                selectedControls.add(control);
        }

        System.out.println("size of controls = " + selectedControls.size());

        // create new controls
        //List<FxControl> fxControlUnderTestList = new ArrayList<>();
        mFxControlUnderTestMap = new HashMap<>();
        int idx = 0;
        for (ControlListModel control : selectedControls) {
            FxControl fxControl = control.getFxControl();
            List<String> testDataList = control.getTestDataList();
            if (testDataList == null) {
                fxControl.setKey(++idx);
                mFxControlUnderTestMap.put(fxControl.getKeyStr(), fxControl);
            } else {
                for (String testData : testDataList) {
                    FxControl newControl = fxControl.copy(testData);
                    newControl.setKey(++idx);
                    mFxControlUnderTestMap.put(newControl.getKeyStr(), newControl);
                }
            }
        }
        System.out.println("size of controlsUT = " + 3);

        // create combinations
        List<String> fxControlKeyList = new ArrayList<>();
        for (String key : mFxControlUnderTestMap.keySet()) {
            fxControlKeyList.add(key);
        }

        FxTestCaseCombinator combinator = new FxTestCaseCombinator();
        List<List<String>> testSequenceList = combinator.combineTestCase(fxControlKeyList, fxControlKeyList.size());

        System.out.println("Num of seq. = " + testSequenceList.size());

        // create test case object
        mTestCaseList.clear();
        for (List<String> testSeq : testSequenceList) {
            StringBuilder sb = new StringBuilder();
            for (String key : testSeq) {
                sb.append(mFxControlUnderTestMap.get(key).toString());
                sb.append(" -> ");
            }
            sb.delete(sb.length() - 4, sb.length());

            FxTestCase newTc = new FxTestCase(testSeq, null);
            mTestCaseList.add(new TestCaseListModel(newTc, sb.toString()));
        }
    }

    @FXML
    private void onGenTestCodeMouseClicked() {
        fxTestCodePath.setText("");
        List<FxTestCase> testCaseList = new ArrayList<>();
        for (TestCaseListModel caseListModel : mTestCaseList) {
            if (caseListModel.isChecked()) {
                testCaseList.add(caseListModel.getFxTestCase());
            }
        }

        FxTestSuite fxTestSuite = new FxTestSuite(mFxControlUnderTestMap, testCaseList);
        String name = mFxmlFile.getName();
        fxTestSuite.setTitle(name.substring(0, name.lastIndexOf(".")) + "Test");
        TestFxCodeGenerator testFxCodeGenerator = new TestFxCodeGenerator();
        try {
            String absolutePath = mFxmlFile.getAbsolutePath();
            int idx = absolutePath.lastIndexOf("\\");
            if (idx < 0) {
                idx = absolutePath.lastIndexOf("/");
            }

            String codePath = testFxCodeGenerator.generateTestCode(fxTestSuite, name.substring(0, name.lastIndexOf(".")), absolutePath.substring(0, idx));
            fxTestCodePath.setText(codePath);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @FXML
    private void onOpenFXMLClicked() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open FXML file");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("FXML", "*.fxml"));

        mFxmlFile = fileChooser.showOpenDialog(null);
        if (mFxmlFile != null) {
            FxmlParser fxmlParser = new FxmlParser();
            addControlList(fxmlParser.parseJavaFxml(mFxmlFile.getAbsolutePath()));
        } else {
            System.out.println("files is null");
        }
    }

    class CaseCheckBoxCell extends TableCell<TestCaseListModel, Boolean> {

        private CheckBox mCheckBox;

        public CaseCheckBoxCell() {
            mCheckBox = new CheckBox();
            mCheckBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
                TestCaseListModel caseListModel = getTableView().getItems().get(getTableRow().getIndex());
                caseListModel.setChecked(newValue);
            });
        }

        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(mCheckBox);
            } else {
                setGraphic(null);
            }
        }
    }

    class CheckBoxCell extends TableCell<ControlListModel, Boolean> {

        private CheckBox mCheckBox;

        public CheckBoxCell() {
            mCheckBox = new CheckBox();
            mCheckBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
                ControlListModel controlListModel = getTableView().getItems().get(getTableRow().getIndex());
                controlListModel.setChecked(newValue);
            });
        }

        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(mCheckBox);
            } else {
                setGraphic(null);
            }
        }
    }

    class BtnTestDataGenCell extends TableCell<ControlListModel, Boolean> {

        private Button mButton;

        public BtnTestDataGenCell() {
            mButton = new Button("Generate");
            mButton.setOnMouseClicked(event -> {
                ControlListModel controlListModel = getTableView().getItems().get(getTableRow().getIndex());
                if (controlListModel.getFxControl().getType() != FxControlType.TEXT_FIELD &&
                        controlListModel.getFxControl().getType() != FxControlType.TEXT_AREA)
                    return;
                try {
                    TestDataGenerationDialog dialog = new TestDataGenerationDialog();
                    List<String> testDataList = dialog.showAndWait();
                    if (testDataList != null) {
                        fxLabelControlInfo.setText(testDataList.toString());
                    }
                    controlListModel.setTestDataList(testDataList);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }

        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if (!empty) {
                setGraphic(mButton);
            } else {
                setGraphic(null);
            }
        }
    }
}
