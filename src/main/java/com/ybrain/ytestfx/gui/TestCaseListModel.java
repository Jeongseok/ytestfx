package com.ybrain.ytestfx.gui;

import com.ybrain.ytestfx.FxTestCase;
import javafx.beans.property.SimpleStringProperty;

/**
 * Created by Ybrain-WONHO on 2016-05-26.
 */
public class TestCaseListModel {
    private SimpleStringProperty testCaseSequence;
    private SimpleStringProperty testCaseResultDemo;

    private FxTestCase mFxTestCase;
    private boolean mChecked;


    public TestCaseListModel(FxTestCase fxTestCase, String seq) {
        this.testCaseSequence = new SimpleStringProperty(seq);
        this.testCaseResultDemo = new SimpleStringProperty(new String(""));
        mFxTestCase = fxTestCase;
        mChecked = false;
    }

    public FxTestCase getFxTestCase() {
        return mFxTestCase;
    }


    public String getTestCaseSequence() {
        return testCaseSequence.get();
    }

    public SimpleStringProperty testCaseSequenceProperty() {
        return testCaseSequence;
    }

    public void setTestCaseSequence(String testCaseSequence) {
        this.testCaseSequence.set(testCaseSequence);
    }

    public String getTestCaseResultDemo() {
        return testCaseResultDemo.get();
    }

    public SimpleStringProperty testCaseResultDemoProperty() {
        return testCaseResultDemo;
    }

    public void setTestCaseResultDemo(String testCaseResultDemo) {
        this.testCaseResultDemo.set(testCaseResultDemo);
        mFxTestCase.mExpectedResultMemo = testCaseResultDemo;
    }

    public void setChecked(Boolean checked) {
        mChecked = checked;
    }

    public boolean isChecked() {
        return mChecked;
    }
}
