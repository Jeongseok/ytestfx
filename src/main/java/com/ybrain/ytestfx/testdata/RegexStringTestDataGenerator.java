package com.ybrain.ytestfx.testdata;

import com.mifmif.common.regex.Generex;

import java.util.List;

/**
 * Created by jeongseok on 2016-05-25.
 */
public class RegexStringTestDataGenerator {
    public static String PATTERN_E_MAIL = "([a-z0-9]+)[@]([a-z0-9]+)[.]([a-z0-9]+)";
    public static String PATTERN_ANY_NUMBER = "(\\d+)";
    public static String PATTERN_ANY_NON_NUMBER = "(\\D+)";
    public static String PATTERN_ANY_WORD = "(\\w+)";
    public static String PATTERN_ANY_NON_WORD = "(\\W+)";
    public static String PATTERN_ANY_TEXT = "(.*)";
    public static String PATTERN_MULTI_CHAR = "[A-Z]{5,9}";

    private String mRegexPattern;
    private Generex mGenerex;
    private int mLength;

    private RegexStringTestDataGenerator(){
    }

    public static RegexStringTestDataGenerator build(String regexPattern, int length){
        RegexStringTestDataGenerator generator = new RegexStringTestDataGenerator();
        if(Generex.isValidPattern(regexPattern)){
            generator.mRegexPattern = regexPattern;
            generator.mGenerex = new Generex(regexPattern);
            generator.mLength = length;
            return generator;
        } else {
            return null;
        }
    }

    public String generate(){
        if(mLength <= 0) {
            return mGenerex.random();
        } else{
            return mGenerex.random(mLength, mLength);
        }
    }

    public String getRegexPattern() {
        return mRegexPattern;
    }
}
