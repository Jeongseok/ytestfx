package com.ybrain.ytestfx;

import com.ybrain.ytestfx.gui.YTestFxMainViewController;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by jeongseok on 2016-05-25.
 */
public class YTestFxApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        YTestFxMainViewController testAppCtl = new YTestFxMainViewController();
        primaryStage.setTitle("Ybrain Automated TestFX Application(ver 0.1)");
        primaryStage.setScene(new Scene(testAppCtl, 1500, 800));
        primaryStage.show();
    }
}