package com.ybrain.ytestfx.test;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by jeongseok on 2016-05-26.
 */
public class TestAppController extends Pane {

    @FXML
    private RadioButton fxRbPlus;

    @FXML
    private RadioButton fxRbDivision;

    @FXML
    private Button fxBtnCalculate;

    @FXML
    private TextField fxTxtSumS;

    @FXML
    private TextField fxTxtSecondN;

    @FXML
    private TextField fxTxtFirstN;

    @FXML
    private CheckBox fxCbFtoB;

    @FXML
    private TextField fxTxtSumN;

    @FXML
    private TextField fxTxtFirstS;

    @FXML
    private Button fxBtnSumString;

    @FXML
    private RadioButton fxRbMultiplication;

    @FXML
    private TextField fxTxtSecondS;

    @FXML
    private RadioButton fxRbMinus;

    @FXML
    private Button fxBtnOk;

    @FXML
    void onBtnNumberCalculateClicked() {
        String txtFirstValue = fxTxtFirstN.getText();
        String txtSecondValue = fxTxtSecondN.getText();

        try{
            int firstValue = Integer.valueOf(txtFirstValue);
            int secondValue = Integer.valueOf(txtSecondValue);
            float result = 0;
            if(fxRbPlus.isSelected()){
                result = firstValue + secondValue;
            } else if(fxRbMinus.isSelected()){
                result = firstValue - secondValue;
            } else if(fxRbMultiplication.isSelected()){
                result = firstValue * secondValue;
            } else if(fxRbDivision.isSelected()){
                result = firstValue / secondValue;
            }
            fxTxtSumN.setText(String.valueOf(result));
        } catch (NumberFormatException e){
            fxTxtSumN.setText("Only insert int values.");
        }
    }

    @FXML
    void onBtnStringCalculateClicked() {
        if(fxCbFtoB.isSelected()){
            fxTxtSumS.setText(fxTxtFirstS.getText() + fxTxtSecondS.getText());
        } else {
            fxTxtSumS.setText(fxTxtSecondS.getText() + fxTxtFirstS.getText());
        }
    }

    @FXML
    void onBtnOkClicked() {
        mStage.close();
    }

    private Stage mStage;

    public TestAppController(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("TestAppView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();

        mStage = stage;
        ToggleGroup rbToggleGroup = new ToggleGroup();
        fxRbPlus.setToggleGroup(rbToggleGroup);
        fxRbMinus.setToggleGroup(rbToggleGroup);
        fxRbMultiplication.setToggleGroup(rbToggleGroup);
        fxRbDivision.setToggleGroup(rbToggleGroup);
    }
}
