import com.ybrain.ytestfx.FxControl;
import com.ybrain.ytestfx.FxControlType;
import com.ybrain.ytestfx.FxTestCase;
import com.ybrain.ytestfx.FxTestSuite;
import com.ybrain.ytestfx.testcode.TestFxCodeGenerator;

import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mswok on 2016-05-25.
 */
public class CodeGeneratorTest {

    @Test
    public void generate() throws IOException {
        TestFxCodeGenerator generator = new TestFxCodeGenerator();
        String cutName = "testCutName";
        String fileName = cutName + "Test.java";

        // Create test controls
        Map<String, FxControl> controlMap = new HashMap<String, FxControl>(10);
        controlMap.put("B1", new FxControl(FxControlType.BUTTON, "okButton"));
        controlMap.put("B2", new FxControl(FxControlType.BUTTON, "cancelButton"));
        controlMap.put("T1", new FxControl(FxControlType.TEXT_FIELD, "contentTextField"));

        FxTestSuite suite = new FxTestSuite(controlMap);

        FxTestCase tc1 = new FxTestCase("First test case. Expected no exception");
        tc1.add("B1");
        tc1.add("B2");
        tc1.add("T1");
        suite.add(tc1);

        FxTestCase tc2 = new FxTestCase("First test case. Expected no exception");
        tc2.add("B2");
        tc2.add("B1");
        tc2.add("T1");
        suite.add(tc2);

        FxTestCase tc3 = new FxTestCase("First test case. Expected no exception");
        tc3.add("T1");
        tc3.add("B1");
        tc3.add("B2");
        suite.add(tc3);

        generator.generateTestCode(suite, cutName, fileName);
    }
}
