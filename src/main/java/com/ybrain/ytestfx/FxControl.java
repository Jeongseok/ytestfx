package com.ybrain.ytestfx;

import java.util.Properties;
import java.util.UUID;

/**
 * Created by jeongseok on 2016-05-25.
 */
public class FxControl {
    private FxControlType mType;
    private String mId;
    private String mValue;
    private Properties mProperties;

    private String mTestData;

    //private UUID mKey;
    private int mKey;

    public FxControl(FxControlType type, String id) {
        this(type, id, null);
    }

    public FxControl(FxControlType type, String id, String value) {
        this(type, id, value, null);
    }

    public FxControl(FxControlType type, String id, String value, Properties properties) {
        mType = type;
        mId = id;
        mValue = value;
        mProperties = properties;

        //mKey = UUID.randomUUID();
    }

    public FxControl(FxControlType type, String id, String value, Properties properties, String testData) {
        mType = type;
        mId = id;
        mValue = value;
        mProperties = properties;
        mTestData = testData;

        //mKey = UUID.randomUUID();
    }

    public FxControl copy(String testData) {
        return new FxControl(this.mType, this.mId, this.mValue, this.mProperties, testData);
    }

    public FxControlType getType() {
        return mType;
    }

    public void setType(FxControlType type) {
        mType = type;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getValue() {
        return mValue;
    }

    public void setValue(String value) {
        mValue = value;
    }

    public Properties getProperties() {
        return mProperties;
    }

    public void setProperties(Properties properties) {
        mProperties = properties;
    }

    public int getKey(){
        return mKey;
   }

    public String getKeyStr(){
        return String.valueOf(mKey);
    }

    public void setKey(int key){
        mKey = key;
    }

    public String getTestData() {
        return mTestData;
    }

    public void setTestData(String testData) {
        mTestData = testData;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(mId);
        sb.append("{");
        if(mTestData != null){
            sb.append(mTestData);
        }
        sb.append("}");
        return sb.toString();
    }
}
