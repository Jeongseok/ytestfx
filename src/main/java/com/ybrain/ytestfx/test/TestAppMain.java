package com.ybrain.ytestfx.test;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by jeongseok on 2016-05-25.
 */



public class TestAppMain extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        TestAppController testAppCtl = new TestAppController(primaryStage);
        primaryStage.setTitle("Test application");
        primaryStage.setScene(new Scene(testAppCtl, 600, 400));
        primaryStage.show();
    }
}