package com.ybrain.ytestfx;

import java.util.ArrayList;
import java.util.List;

public class FxTestCase {
    public List<String> mKeys = new ArrayList<String>(0);
    public String mExpectedResultMemo;

    public FxTestCase(List<String> keys, String expectedResultMemo) {
        mKeys = keys;
        mExpectedResultMemo = expectedResultMemo;
    }

    public FxTestCase(String expectedResultMemo) {
        mExpectedResultMemo = expectedResultMemo;
    }

    public int size() {
        return mKeys.size();
    }

    public boolean isEmpty() {
        return mKeys.isEmpty();
    }

    public String get(int index) {
        return mKeys.get(index);
    }

    public void add(String element) {
        mKeys.add(element);
    }
}
