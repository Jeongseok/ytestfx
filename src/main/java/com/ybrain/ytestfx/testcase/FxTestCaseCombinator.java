package com.ybrain.ytestfx.testcase;

import com.rmn.pairwise.IInventory;
import com.rmn.pairwise.PairwiseInventoryFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by ldtuan on 2016-05-25.
 * <p>
 * Research shows that testing all pairs is an effective alternative to exhaustive testing
 * and much less costly. It will provide very good coverage and the number of test cases will remain
 * manageable.
 */
public class FxTestCaseCombinator {
    private final int MAX_TEST_LENGTH = 6;
    private static final Logger LOGGER = LoggerFactory.getLogger(FxTestCaseCombinator.class);
    /**
     * A pairwise generator
     *
     * @param valueList      value list(It is unique key of FxControl) for combination
     * @param sequenceLength length of a output <code>List<String></code>, i.e., length of a test case's sequence
     * @return combination list
     */

    public List<List<String>> combineTestCase(List<String> valueList, int sequenceLength) {
        if (valueList.size() < sequenceLength)
            throw new IllegalArgumentException("SequenceLength must be smaller than size of the valueList");

        Set<String> set = new HashSet<String>(valueList);
        if (set.size() < valueList.size())
            throw new IllegalArgumentException("There is some duplicated Ids in the valueList");

        sequenceLength = (sequenceLength < MAX_TEST_LENGTH) ? sequenceLength: MAX_TEST_LENGTH;

        //Naming Sequence length parameters
        List<String> parameters = new ArrayList<String>();
        for (int i = 1; i <= sequenceLength; i++) {
            parameters.add("P" + i);
        }

        StringBuilder scenarios = new StringBuilder();

        String lineBreak = "";
        for (String param : parameters) {
            scenarios.append(lineBreak);
            lineBreak = System.lineSeparator();
            scenarios.append(param);
            scenarios.append(": ");

            String comma = "";
            for (String value : valueList) {
                scenarios.append(comma);
                comma = ", ";
                scenarios.append(value);
            }
        }
        LOGGER.info("---------------------Parameters and values--------------------\n" + scenarios.toString());

        IInventory inventory = PairwiseInventoryFactory.generateParameterInventory(scenarios.toString());
        List<Map<String, String>> rawDataSet = inventory.getTestDataSet().getTestSets();

        List<List<String>> results = new ArrayList<List<String>>();
        for (Map<String, String> rawTestCase : rawDataSet) {
            List<String> oneCase = new ArrayList<String>();
            for (String param : parameters) {
                oneCase.add(rawTestCase.get(param));
            }
            results.add(oneCase);
        }

        LOGGER.info("Total possible test cases     : " + (int) java.lang.Math.pow(valueList.size(), sequenceLength));
        LOGGER.info("Number of pairwise test cases : " + results.size());
        for (List<String> testCase : results) {
            LOGGER.info(testCase.toString());
        }

        return results;
    }
}
