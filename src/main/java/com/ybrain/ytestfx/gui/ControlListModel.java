package com.ybrain.ytestfx.gui;

import com.ybrain.ytestfx.FxControl;
import javafx.beans.property.SimpleStringProperty;

import java.util.List;

/**
 * Created by Ybrain-WONHO on 2016-05-25.
 */
public class ControlListModel {
    private SimpleStringProperty controlType;
    private SimpleStringProperty controlId;

    private FxControl mFxControl;
    private boolean mIsChecked;
    private List<String> mTestDataList;

    public void setTestDataList(List<String> testDataList) {
        mTestDataList = testDataList;
    }

    public List<String> getTestDataList() {
        return mTestDataList;
    }

    public ControlListModel(FxControl fxControl) {
        this.controlType = new SimpleStringProperty(fxControl.getType().getTypeValue());
        this.controlId = new SimpleStringProperty(fxControl.getId());
        mFxControl = fxControl;
        mIsChecked = false;
        mTestDataList = null;
    }

    public String getControlType() {
        return controlType.get();
    }

    public SimpleStringProperty controlTypeProperty() {
        return controlType;
    }

    public void setControlType(String controlType) {
        this.controlType.set(controlType);
    }

    public String getControlId() {
        return controlId.get();
    }

    public SimpleStringProperty controlIdProperty() {
        return controlId;
    }

    public void setControlId(String controlId) {
        this.controlId.set(controlId);
    }

    public boolean isChecked() {
        return mIsChecked;
    }

    public void setChecked(boolean checked) {
        mIsChecked = checked;
    }

    public FxControl getFxControl() {
        return mFxControl;
    }

    public void setFxControl(FxControl fxControl) {
        mFxControl = fxControl;
    }
}
