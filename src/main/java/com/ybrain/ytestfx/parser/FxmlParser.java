package com.ybrain.ytestfx.parser;

import com.ybrain.ytestfx.FxControl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.ybrain.ytestfx.FxControlType;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;

/**
 * Created by jeongseok on 2016-05-25.
 */
public class FxmlParser {

    private ArrayList<FxControlType> mControlTypeValueList = new ArrayList<FxControlType>();
    private List<FxControl> mFxControlList ;

    public FxmlParser(){
        setControlTypeValue();
    }

    /**
     * @param file FXML file with path
     * @return List of FxControl
     */
    public List<FxControl> parseJavaFxml(String file) {
        mFxControlList = new ArrayList<FxControl>();
        try {
            File inputFile = new File(file);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();

            if (doc.hasChildNodes()) {
                printNode(doc.getChildNodes());
            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return mFxControlList;
    }

    private void setControlTypeValue(){
        mControlTypeValueList.add(FxControlType.BUTTON);
        mControlTypeValueList.add(FxControlType.CHECK_BOX);
        mControlTypeValueList.add(FxControlType.CHOICE_BOX);
        mControlTypeValueList.add(FxControlType.COLOR_PICKER);
        mControlTypeValueList.add(FxControlType.COMBO_BOX);
        mControlTypeValueList.add(FxControlType.DATE_PICKER);
        mControlTypeValueList.add(FxControlType.HTML_EDITOR);
        mControlTypeValueList.add(FxControlType.HYPER_LINK);
        mControlTypeValueList.add(FxControlType.IMAGE_VIEW);

        mControlTypeValueList.add(FxControlType.LABEL);
        mControlTypeValueList.add(FxControlType.LIST_VIEW);
        mControlTypeValueList.add(FxControlType.MEDIA_VIEW);
        mControlTypeValueList.add(FxControlType.MENU_BAR);
        mControlTypeValueList.add(FxControlType.MENU_BUTTON);
        mControlTypeValueList.add(FxControlType.PAGINATION);

        mControlTypeValueList.add(FxControlType.PASSWORD_FIELD);
        mControlTypeValueList.add(FxControlType.PROGRESS_BAR);
        mControlTypeValueList.add(FxControlType.PROGRESS_INDICATOR);

        mControlTypeValueList.add(FxControlType.RADIO_BUTTON);
        mControlTypeValueList.add(FxControlType.SCROLL_BAR);
        mControlTypeValueList.add(FxControlType.SEPARATOR);
        mControlTypeValueList.add(FxControlType.SLIDER);
        mControlTypeValueList.add(FxControlType.SPLIT_MENU_BUTTON);

        mControlTypeValueList.add(FxControlType.TABLE_COLUMN);
        mControlTypeValueList.add(FxControlType.TABLE_VIEW);

        mControlTypeValueList.add(FxControlType.TEXT_FIELD);
        mControlTypeValueList.add(FxControlType.TEXT_AREA);

        mControlTypeValueList.add(FxControlType.TOGGLE_BUTTON);

        mControlTypeValueList.add(FxControlType.TREE_TABLE_COLUMN);
        mControlTypeValueList.add(FxControlType.TREE_TABLE_VIEW);
        mControlTypeValueList.add(FxControlType.TREE_VIEW);
        mControlTypeValueList.add(FxControlType.WEB_VIEW);
    }

    private void printNode(NodeList nodeList) {
        for (int count = 0; count < nodeList.getLength(); count++) {
            Node tempNode = nodeList.item(count);

            FxControl fxControl;
            for(FxControlType fxControlType : mControlTypeValueList){
                if(fxControlType.getTypeValue()==tempNode.getNodeName()){
                    String id = ((Element) tempNode).getAttribute("fx:id");
                    String value = ((Element) tempNode).getAttribute("text");
                    if(!id.equals("") && !value.equals("")){
                        fxControl = new FxControl(fxControlType, id, value);
                    } else if (!id.equals("")){
                        fxControl = new FxControl(fxControlType, id);
                    } else {
                        return;
                    }
                    mFxControlList.add(fxControl);
                }
            }
            if (tempNode.hasChildNodes()) {
                // loop again if has child nodes
                printNode(tempNode.getChildNodes());
            }
        }
    }
}

