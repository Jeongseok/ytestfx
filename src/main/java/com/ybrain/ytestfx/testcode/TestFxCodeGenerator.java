package com.ybrain.ytestfx.testcode;

import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import com.ybrain.ytestfx.FxControl;
import com.ybrain.ytestfx.FxControlType;
import com.ybrain.ytestfx.FxTestCase;
import com.ybrain.ytestfx.FxTestSuite;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.lang.model.element.Modifier;

import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by Sangwoo-Maeng on 2016-05-25.
 */
public class TestFxCodeGenerator {
    private static final HashMap<FxControlType, StatementCreator> STMT_MAP = new HashMap<FxControlType, StatementCreator>();

    /**
     * @param fxTestSuite test suite
     * @param cutName     name of class(It is FXML controller class) under test
     * @param file        file name with path of generated source code
     * @return
     */
    public String generateTestCode(FxTestSuite fxTestSuite, String cutName, String file) throws IOException {
        TypeSpec.Builder classBuilder = TypeSpec.classBuilder(fxTestSuite.getTitle())
                .superclass(ApplicationTest.class)
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .addAnnotation(FixMethodOrder.class);

        // @override void start()
        classBuilder.addMethod(MethodSpec.methodBuilder("start")
                .addAnnotation(Override.class)
                .addModifiers(Modifier.PUBLIC)
                .returns(void.class)
                .addParameter(Stage.class, "stage")
                .addStatement("TestAppController testAppCtl = new TestAppController(stage)")
                .addStatement("stage.setTitle(\"Test application\")")
                .addStatement("stage.setScene(new $T(testAppCtl, 600, 400))", Scene.class)
                .addStatement("stage.show()")
                .addException(IOException.class)
                .build());

        int idx = 0;
        for (FxTestCase testCase : fxTestSuite.mTestCases) {
            MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder("tc_" + idx++)
                    .addAnnotation(Test.class)
                    .addModifiers(Modifier.PUBLIC)
                    .returns(void.class);

            for (String key : testCase.mKeys) {
                FxControl fxControl = fxTestSuite.mFxControlMap.get(key);
                createStatement(fxControl, methodBuilder);
            }

            if (testCase.mExpectedResultMemo != null) {
                methodBuilder.addCode("\n");
                methodBuilder.addCode("//" + testCase.mExpectedResultMemo+"\n");
            }
            classBuilder.addMethod(methodBuilder.build());
        }

        JavaFile javaFile = JavaFile.builder("", classBuilder.build()).build();
        javaFile.writeTo(new File(file));
        return file;
    }

    private void createStatement(FxControl fxControl, MethodSpec.Builder methodBuilder) {
        StatementCreator creator = STMT_MAP.get(fxControl.getType());
        if (creator != null) {
            creator.create(fxControl, methodBuilder);
        } else {
            methodBuilder.addCode("// Not supported control : " + fxControl.getType());
        }
    }

    private interface StatementCreator {
        void create(FxControl fxControl, MethodSpec.Builder methodBuilder);
    }

    private static final StatementCreator CODE_CLICK = new StatementCreator() {
        @Override
        public void create(FxControl fxControl, MethodSpec.Builder methodBuilder) {
            methodBuilder.addStatement("clickOn($S)", "#" + fxControl.getId());
        }
    };

    static {
        STMT_MAP.put(FxControlType.BUTTON, CODE_CLICK);
        STMT_MAP.put(FxControlType.CHECK_BOX, CODE_CLICK);
        STMT_MAP.put(FxControlType.RADIO_BUTTON, CODE_CLICK);
        STMT_MAP.put(FxControlType.TEXT_FIELD, new StatementCreator() {
            @Override
            public void create(FxControl fxControl, MethodSpec.Builder methodBuilder) {
                methodBuilder.addStatement("clickOn($S).write( $S)", "#" + fxControl.getId(), fxControl.getTestData());
            }
        });
    }
}
