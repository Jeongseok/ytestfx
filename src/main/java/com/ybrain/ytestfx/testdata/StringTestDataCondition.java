package com.ybrain.ytestfx.testdata;

/**
 * Created by jeongseok on 2016-05-25.
 */
public class StringTestDataCondition {
    private boolean mIsNumber;
    private boolean mIsUpperAlphabet;
    private boolean mIsLowerAlphabet;
    private boolean mSpecialCharacters;

    public StringTestDataCondition(){
        mIsNumber = false;
        mIsUpperAlphabet = false;
        mIsLowerAlphabet = false;
        mSpecialCharacters = false;
    }

    public boolean isNumber() {
        return mIsNumber;
    }

    public void setNumber(boolean number) {
        mIsNumber = number;
    }

    public boolean isUpperAlphabet() {
        return mIsUpperAlphabet;
    }

    public void setUpperAlphabet(boolean upperAlphabet) {
        mIsUpperAlphabet = upperAlphabet;
    }

    public boolean isLowerAlphabet() {
        return mIsLowerAlphabet;
    }

    public void setLowerAlphabet(boolean lowerAlphabet) {
        mIsLowerAlphabet = lowerAlphabet;
    }

    public boolean isSpecialCharacters() {
        return mSpecialCharacters;
    }

    public void setSpecialCharacters(boolean specialCharacters) {
        mSpecialCharacters = specialCharacters;
    }

    public boolean isAllFalse(){
        if(!mIsNumber && !mIsLowerAlphabet && !mIsUpperAlphabet && mSpecialCharacters)
            return true;
        else
            return false;
    }
}
