package com.ybrain.ytestfx;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by jeongseok on 2016-05-25.
 */
public class FxTestSuite {
    private String mTitle = "YFxTestSuite";
    public List<FxTestCase> mTestCases = new ArrayList<FxTestCase>(0);
    public Map<String, FxControl> mFxControlMap;

    public FxTestSuite(Map<String, FxControl> fxControlMap, List<FxTestCase> testCases) {
        mTestCases = testCases;
        mFxControlMap = fxControlMap;
    }

    public FxTestSuite(Map<String, FxControl> fxControlMap) {
        mFxControlMap = fxControlMap;
    }

    public boolean add(FxTestCase fxTestCase) {
        return mTestCases.add(fxTestCase);
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }
}
