import com.ybrain.ytestfx.test.TestAppController;

import org.junit.Test;
import org.testfx.framework.junit.ApplicationTest;

import java.io.IOException;

import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by jeongseok on 2016-05-26.
 */
public class LoginTest extends ApplicationTest {
    @Override
    public void start(Stage stage) {
        TestAppController testAppCtl = null;
        try {
            testAppCtl = new TestAppController(stage);
            stage.setTitle("Test application");
            stage.setScene(new Scene(testAppCtl, 600, 400));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void should_drag_file_into_trashcan() {
        // given:
        //clickOn("fxTxtSumN").write("jeong seok akng");

        clickOn("#fxRbMinus");
        clickOn("#fxRbPlus");
        clickOn("#fxTxtFirstN").write("temp");
        clickOn("#fxRbMinus");
        clickOn("#fxCbFtoB");

        /*rightClickOn("#desktop").moveTo("New").clickOn("Text Document");
        write("myTextfile.txt").push(ENTER);

        // when:
        drag(".file").dropTo("#trash-can");

        // then:
        verifyThat("#desktop", hasChildren(0, ".file"));*/
    }
}