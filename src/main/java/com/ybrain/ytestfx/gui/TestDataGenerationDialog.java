package com.ybrain.ytestfx.gui;

import com.ybrain.ytestfx.testdata.RandomStringTestDataGenerator;
import com.ybrain.ytestfx.testdata.RegexStringTestDataGenerator;
import com.ybrain.ytestfx.testdata.StringTestDataCondition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by jeongseok on 2016-05-25.
 */
public class TestDataGenerationDialog extends Pane implements Initializable {
    @FXML
    private RadioButton fxRegular;

    @FXML
    private CheckBox fxSpecialChar;

    @FXML
    private Button fxBtnCancel;

    @FXML
    private CheckBox fxUpperAlp;

    @FXML
    private TextField fxLengString;

    @FXML
    private TextField fxNumTestData;

    @FXML
    private TextField fxExpression;

    @FXML
    private CheckBox fxNumber;

    @FXML
    private TextArea fxGenTestData;

    @FXML
    private RadioButton fxRandom;

    @FXML
    private CheckBox fxLowerAlp;

    @FXML
    private Button fxBtnGen;

    @FXML
    private Button fxBtnOk;

    @FXML
    private RadioButton fxRegAnyText;
    @FXML
    private RadioButton fxRegAnyWord;
    @FXML
    private RadioButton fxRegAnyNoNumber;
    @FXML
    private RadioButton fxRegEmail;
    @FXML
    private RadioButton fxRegAnyNonWord;
    @FXML
    private RadioButton fxRegAnyNumber;
    @FXML
    private RadioButton fxRegMultiChar;

    private Stage mStage;
    private List<String> mGeneratedTestDataList;

    public TestDataGenerationDialog() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("TestDataGenerationView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        fxmlLoader.load();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        mStage = new Stage();
        mStage.initModality(Modality.APPLICATION_MODAL);
        mStage.initStyle(StageStyle.TRANSPARENT);

        ToggleGroup toggleGroup = new ToggleGroup();
        fxRandom.setToggleGroup(toggleGroup);
        fxRegular.setToggleGroup(toggleGroup);

        ToggleGroup regGroup = new ToggleGroup();
        fxRegAnyNoNumber.setToggleGroup(regGroup);
        fxRegAnyNumber.setToggleGroup(regGroup);
        fxRegAnyNonWord.setToggleGroup(regGroup);
        fxRegAnyWord.setToggleGroup(regGroup);
        fxRegMultiChar.setToggleGroup(regGroup);
        fxRegAnyText.setToggleGroup(regGroup);
        fxRegEmail.setToggleGroup(regGroup);


        fxRandom.setSelected(true);
        fxRegular.setSelected(false);
        selectedRandomBtn(true);
        fxGenTestData.setDisable(false);

        fxRandom.selectedProperty().addListener((observable, oldValue, newValue) -> {
            selectedRandomBtn(newValue);
        });
        fxRegular.selectedProperty().addListener((observable, oldValue, newValue) -> {
            selectedRandomBtn(!newValue);
        });

        fxRegAnyNoNumber.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                fxExpression.setText(RegexStringTestDataGenerator.PATTERN_ANY_NON_NUMBER);
            }

        });
        fxRegAnyNumber.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                fxExpression.setText(RegexStringTestDataGenerator.PATTERN_ANY_NUMBER);
            }

        });
        fxRegAnyNonWord.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                fxExpression.setText(RegexStringTestDataGenerator.PATTERN_ANY_NON_WORD);
            }

        });
        fxRegAnyWord.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                fxExpression.setText(RegexStringTestDataGenerator.PATTERN_ANY_WORD);
            }

        });

        fxRegMultiChar.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                fxExpression.setText(RegexStringTestDataGenerator.PATTERN_MULTI_CHAR);
            }

        });
        fxRegAnyText.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                fxExpression.setText(RegexStringTestDataGenerator.PATTERN_ANY_TEXT);
            }

        });
        fxRegEmail.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                fxExpression.setText(RegexStringTestDataGenerator.PATTERN_E_MAIL);
            }
        });
    }

    private void selectedRandomBtn(boolean select) {
        fxNumber.setDisable(!select);
        fxSpecialChar.setDisable(!select);
        fxUpperAlp.setDisable(!select);
        fxLowerAlp.setDisable(!select);

        fxExpression.setDisable(select);
        fxRegAnyNoNumber.setDisable(select);
        fxRegAnyNumber.setDisable(select);
        fxRegAnyNonWord.setDisable(select);
        fxRegAnyWord.setDisable(select);
        fxRegMultiChar.setDisable(select);
        fxRegAnyText.setDisable(select);
        fxRegEmail.setDisable(select);
    }

    public List<String> showAndWait() {
        mStage.setScene(new Scene(this));
        mStage.getScene().setFill(null);
        mStage.centerOnScreen();
        mStage.showAndWait();

        return mGeneratedTestDataList;
    }

    @FXML
    void onGenerateBtnMouseClicked() {
        mGeneratedTestDataList = new ArrayList<>();
        int numTestData = 1;
        if (fxNumTestData.getText() != null && !fxNumTestData.getText().equals("")) {
            numTestData = Integer.valueOf(fxNumTestData.getText());
        }
        int lengthString = 5;
        if (fxLengString.getText() != null && !fxLengString.getText().equals("")) {
            lengthString = Integer.valueOf(fxLengString.getText());
        }

        if (fxRandom.isSelected()) {
            StringTestDataCondition condition = new StringTestDataCondition();
            if (fxNumber.isSelected()) {
                condition.setNumber(true);
            }
            if (fxSpecialChar.isSelected()) {
                condition.setSpecialCharacters(true);
            }
            if (fxUpperAlp.isSelected()) {
                condition.setUpperAlphabet(true);
            }
            if (fxLowerAlp.isSelected()) {
                condition.setLowerAlphabet(true);
            }
            RandomStringTestDataGenerator generator = RandomStringTestDataGenerator.build(condition, lengthString);
            for (int i = 0; i < numTestData; i++) {
                mGeneratedTestDataList.add(generator.generate());
            }

        } else {
            RegexStringTestDataGenerator generator = RegexStringTestDataGenerator.build(fxExpression.getText(), lengthString);
            for (int i = 0; i < numTestData; i++) {
                mGeneratedTestDataList.add(generator.generate());
            }
        }

        printTestDataList(mGeneratedTestDataList);
    }

    private void printTestDataList(List<String> testDataList) {
        StringBuilder sb = new StringBuilder();
        for (String td : testDataList) {
            sb.append(td);
            sb.append(", ");
        }
        fxGenTestData.setText(sb.toString());
    }

    @FXML
    void onOkBtnMouseClicked() {
        mStage.close();
    }

    @FXML
    void onCancelBtnMouseClicked() {
        mGeneratedTestDataList = null;
        mStage.close();
    }
}