package com.ybrain.ytestfx;

/**
 * Created by jeongseok on 2016-05-25.
 */
public enum FxControlType {
    BUTTON("Button"),
    CHECK_BOX("CheckBox"),
    CHOICE_BOX("ChoiceBox"),
    COLOR_PICKER("ColorPicker"),
    COMBO_BOX("ComboBox"),
    DATE_PICKER("DatePicker"),
    HTML_EDITOR("HTMLEditor"),
    HYPER_LINK("Hyperlink"),
    IMAGE_VIEW("ImageView"),

    LABEL("Label"),
    LIST_VIEW("ListView"),
    MEDIA_VIEW("MediaView"),
    MENU_BAR("MenuBar"),
    MENU_BUTTON("MenuButton"),
    PAGINATION("Pagination"),

    PASSWORD_FIELD("PasswordField"),
    PROGRESS_BAR("ProgressBar"),
    PROGRESS_INDICATOR("ProgressIndicator"),

    RADIO_BUTTON("RadioButton"),
    SCROLL_BAR("ScrollBar"),
    SEPARATOR("Separator"),
    SLIDER("Slider"),
    SPLIT_MENU_BUTTON("SplitMenuButton"),

    TABLE_COLUMN("TableColumn"),
    TABLE_VIEW("TableView"),

    TEXT_FIELD("TextField"),
    TEXT_AREA("TextArea"),

    TOGGLE_BUTTON("ToggleButton"),

    TREE_TABLE_COLUMN("TreeTableColumn"),
    TREE_TABLE_VIEW("TreeTableView"),
    TREE_VIEW("TreeView"),
    WEB_VIEW("WebView");

    private String mTypeValue;

    FxControlType(String typeValue){
        mTypeValue = typeValue;
    }

    public String getTypeValue(){
        return mTypeValue;
    }
}
