import com.rmn.pairwise.IInventory;
import com.rmn.pairwise.PairwiseInventoryFactory;
import com.ybrain.ytestfx.testcase.FxTestCaseCombinator;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by tommy on 5/25/2016.
 */
public class PairwiseCombinatorTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(PairwiseCombinatorTest.class);

    private static final String NAV_SCENARIO =
            "Browser: Chrome, Firefox, InternetExplorer, Safari"
                    + "\nPage: Home, Category, Search, New Products"
                    + "\nProduct: Phone, Movie, Computer, Blender, Microwave, Book, Sweater"
                    + "\nClick: Link, Image, Description"
                    + "\nClick2: Link2, Image2, Description2";

    @Test
    public void buildTestSets() {
        //First, generate the list of vectors we *want*
        IInventory inventory = PairwiseInventoryFactory.generateParameterInventory(NAV_SCENARIO);
        List<Map<String, String>> rawDataSet = inventory.getTestDataSet().getTestSets();

        //Now, go through the vectors in the database to figure out what we already *have*
        // If we don't have it already, create it
        for (Map<String, String> rawTestCase : rawDataSet) {
            LOGGER.debug(rawTestCase.get("Browser")
                    + ", " + rawTestCase.get("Page")
                    + ", " + rawTestCase.get("Product")
                    + ", " + rawTestCase.get("Click")
                    + ", " + rawTestCase.get("Click2"));
        }
    }

    @Test
    public void testCombinator() {
        List<String> valueList = new ArrayList<String>();
        valueList.add("Textbox001");
        valueList.add("Textbox002");
        valueList.add("Textbox003");
        valueList.add("Combo001");
        valueList.add("Combo002");
        valueList.add("Button003");
        valueList.add("Button004");
        FxTestCaseCombinator combinator = new FxTestCaseCombinator();
        List<List<String>> testCases = combinator.combineTestCase(valueList, 5);
    }

}
