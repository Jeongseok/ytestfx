package com.ybrain.ytestfx.testdata;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

/**
 * Created by jeongseok on 2016-05-25.
 */
public class RandomStringTestDataGenerator {
    private static final String NUMBERS = "0123456789";
    private static final String UPPER_ALPHABETS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String LOWER_ALPHABETS = "abcdefghijklmnopqrstuvwxyz";
    private static final String SPECIAL_CHARACTERS = "@#$%&*";

    private static final String RANDOM_ALGORITHM = "SHA1PRNG";
    private static final int DEFAULT_LENGTH = 10;

    private char[] mSymbols;
    private SecureRandom mSecureRandom;
    private int mLength;

    private RandomStringTestDataGenerator(){

    }

    public static RandomStringTestDataGenerator build(StringTestDataCondition condition) {
        return build(condition, DEFAULT_LENGTH);
    }

    public static RandomStringTestDataGenerator build(StringTestDataCondition condition, int length) {
        RandomStringTestDataGenerator generator = new RandomStringTestDataGenerator();
        generator.mSymbols = generator.generateSymbols(condition).toCharArray();
        try {
            generator.mSecureRandom = SecureRandom.getInstance(RANDOM_ALGORITHM);
            generator.mLength = length;
            return generator;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String generate() {
        return generate(mLength);
    }

    public String generate(int length){
        char[] buf = new char[length];
        for(int idx = 0; idx < length; idx++){
            buf[idx] = mSymbols[mSecureRandom.nextInt(mSymbols.length)];
        }
        return new String(buf);
    }

    private String generateSymbols(StringTestDataCondition condition){
        StringBuilder sbCharSet = new StringBuilder();
        if(condition.isNumber())
            sbCharSet.append(NUMBERS);
        if(condition.isLowerAlphabet())
            sbCharSet.append(LOWER_ALPHABETS);
        if(condition.isUpperAlphabet())
            sbCharSet.append(UPPER_ALPHABETS);
        if(condition.isSpecialCharacters())
            sbCharSet.append(SPECIAL_CHARACTERS);
        return sbCharSet.toString();
    }
}
