import org.junit.FixMethodOrder;
import org.junit.Test;
import org.testfx.api.FxAssert;
import org.testfx.framework.junit.ApplicationTest;
import org.testfx.matcher.control.LabeledMatchers;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

@FixMethodOrder
public class DesktopPaneTest extends ApplicationTest {
    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("com/ybrain/ytestfx/test/TestAppView.fxml"));
        Scene scene = new Scene(root, 800, 600);
        stage.setScene(scene);
        stage.show();
    }

    @Test
    public void hasText() throws InterruptedException {
        clickOn("#nameInput").write("");
        FxAssert.verifyThat("#nameInput", LabeledMatchers.hasText("steve"));
    }
}